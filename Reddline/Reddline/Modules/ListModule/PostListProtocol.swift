//
//  PostListProtocol.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

typealias downloadImageCompletionHandler = (UIImage) -> Void
typealias queryCompletionWithErrorMessage = (String) -> Void

protocol PostListViewControllerProtocol: class {
    
    var selectedPost: PostModel? { set get }
    
    func didLoadData()
    
    func showLoading()
    
    func hideLoading()
    
    func showMessage(title: String!, description: String!)
    
    func insertRows(at indexPaths: [IndexPath])
    
    func scrollTo(index: Int)
}

protocol PostListPresenterProtocol: class {
    
    var router: PostListRouterProtocol! { set get }
    
    var posts: [PostModel] { set get }
    
    func configureView()
    
    func reload()
    
    func configureCell(cell: PostCell, post: PostModel)
    
    func endDisplayCell(cell: PostCell, post: PostModel)
    
    func setLastVisiblePosts(posts: [PostModel])
    
    func didLoadData(data: [PostModel])
    
    func didLoadNextData(data: [PostModel])
    
    func onError(message: String!)
    
    func didScrollToEnd()
    
    func canShowDetail(for post: PostModel) -> Bool
}

protocol PostListInteractorProtocol: class {
    
    var apiManager: RedditAPI! { set get }
    
    var localDataManager: LocalDataManager! { set get }
    
    func loadData()
    
    func loadNextPageData()
    
    func getImageAsync(from url: URL, success: @escaping downloadImageCompletionHandler, failure: @escaping queryCompletionWithErrorMessage)
    
    func cancelLoadImage(for url: URL)
}

protocol PostListRouterProtocol: class {
    
    func showDetailScene()
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
}

protocol PostWireframeProtocol: class {

    func configureModule(viewController: PostListViewController)
}
