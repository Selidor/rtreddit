//
//  PostCell.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {

    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var postTitleLabel: UILabel!
    @IBOutlet weak var createdDateLabel: UILabel!
    @IBOutlet weak var commentsNumLabel: UILabel!
    var imageURI: String?
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        previewImage.image = nil;
        showPreview()
    }
    
    func hidePreview(){
        heightConstraint?.constant = 0
    }
    
    func showPreview(){
        heightConstraint?.constant = 200
    }
    
    func configure(with author: String,
                   title: String,
                   localizedDateString: String,
                   numCommentsString: String) {
        
        authorLabel.text = author
        postTitleLabel.text = title
        createdDateLabel.text = localizedDateString
        commentsNumLabel.text = "Comments: " + numCommentsString
    }

}
