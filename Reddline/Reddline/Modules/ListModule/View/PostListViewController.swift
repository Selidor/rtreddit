//
//  PostListViewController.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

class PostListViewController: UIViewController, PostListViewControllerProtocol {

    @IBOutlet weak var tableView: UITableView!
    private var refreshControl = UIRefreshControl()
    private let coordinator: (PostWireframeProtocol & PostListRouterProtocol) = PostListCoordinator()
    var presenter: PostListPresenter!
    var selectedPost: PostModel?


    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        coordinator.configureModule(viewController: self)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        tableView.addSubview(refreshControl)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100.0
        tableView.tableFooterView = UIView()
        definesPresentationContext = true
        
        presenter.configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.barStyle = .black
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        presenter.router .prepare(for: segue, sender: sender)
    }
    
    override func encodeRestorableState(with coder: NSCoder) {
        
        if (presenter.posts.count > 0) {
            
            var visiblePosts: [PostModel] = []
            let visibleIndexPaths = tableView.indexPathsForVisibleRows!
            for ips in  visibleIndexPaths {
                let post = presenter.posts[ips.row]
                visiblePosts.append(post)
            }
            coder.encode(visiblePosts, forKey: "lastVisiblePosts")
            super.encodeRestorableState(with: coder)
        }
        
        super.encodeRestorableState(with: coder)
    }
    
    override func decodeRestorableState(with coder: NSCoder) {
        
        let lastVisiblePosts = (coder.decodeObject(forKey: "lastVisiblePosts") as? [PostModel])
        if lastVisiblePosts != nil {
            presenter.setLastVisiblePosts(posts: lastVisiblePosts!)
        }
        
        super.decodeRestorableState(with: coder)
    }
    
    
    @objc func refresh(){
        presenter.reload()
    }
    
    func didLoadData() {
        tableView.reloadData()
    }
    
    func insertRows(at indexPaths: [IndexPath]){
        tableView.insertRows(at: indexPaths, with: UITableView.RowAnimation.bottom)
    }
    
    func showLoading() {
        refreshControl.beginRefreshing()
    }
    
    func hideLoading() {
        
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }
    
    func showMessage(title: String!, description: String!) {
        
        if ((self.navigationController?.visibleViewController?.isKind(of: UIAlertController.self)) == false) {
            
            weak var weakSelf = self
            
            let alertController = UIAlertController(title: title, message:
                description, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { action in
                weakSelf?.refreshControl.endRefreshing()
            }))
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func scrollTo(index: Int) {
        if index > 0 {
            tableView.scrollToRow(at: IndexPath.init(row: index, section: 0), at: UITableView.ScrollPosition.top, animated: false)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
}

extension PostListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.posts.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        
        if(presenter.posts.count > 0){
            
            let post = presenter.posts[indexPath.row]
            presenter.configureCell(cell: cell, post: post)
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(presenter.posts.count > 0){
            
            let posts = presenter.posts
            selectedPost = posts[indexPath.row]
            
            if presenter.canShowDetail(for: selectedPost!){
                
                performSegue(withIdentifier: "showDetail", sender: self)
                           tableView.deselectRow(at: indexPath, animated: false)
            }else{
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if presenter.posts.count > 0, indexPath.row == (presenter.posts.count - 1){
            print("didScrollToEnd")
            presenter.didScrollToEnd()
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let post = presenter.posts[indexPath.row]
        presenter.endDisplayCell(cell: cell as! PostCell, post: post)
    }
    
}
extension PostListViewController: UIViewControllerRestoration{
    
    static func viewController(withRestorationIdentifierPath identifierComponents: [String], coder: NSCoder) -> UIViewController? {
        let vc = PostListViewController()
        vc.showMessage(title: "TITLE", description: "Restoration")
        return vc
    }
}



