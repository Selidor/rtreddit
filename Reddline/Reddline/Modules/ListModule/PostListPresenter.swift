//
//  PostListPresenter.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

class PostListPresenter: PostListPresenterProtocol {
    
    var posts: [PostModel] = []
    var lastVisiblePosts: [PostModel]?
    weak var router: PostListRouterProtocol!
    weak var view: PostListViewControllerProtocol!
    var interactor: PostListInteractorProtocol!
    var isReload: Bool!
    
    required init(view: PostListViewControllerProtocol) {
        self.view = view
    }
    
    func configureView() {
        
        isReload = false
        view.showLoading()
        interactor.loadData()
    }
    
    func reload(){
        
        isReload = true
        view.showLoading()
        interactor.loadData()
    }
    
    func didLoadData(data: [PostModel]) {
        
        view.hideLoading()
        posts = data
        view.didLoadData()
        if(isReload == false){
            findLastVisiblePosts()
        }
    }
    
    
    func didLoadNextData(data: [PostModel]) {
        
        view.hideLoading()
        
        var indexPaths = [IndexPath]()
        for index in posts.count...(posts.count + data.count - 1) {
            let ip = IndexPath.init(row: index, section: 0)
            indexPaths.append(ip)
        }
        
        posts.append(contentsOf: data)
        view.insertRows(at: indexPaths)
    }
    
    func onError(message: String!) {
        view.showMessage(title: "Error", description: message)
    }
    
    func configureCell(cell: PostCell, post: PostModel) {
        
        let formatter = RelativeDateTimeFormatter()
        let localizedDateString = formatter.localizedString(for: post.created, relativeTo: Date.init())
        cell.configure(with: post.author!,
                       title: post.title!,
                       localizedDateString: localizedDateString,
                       numCommentsString: String(post.numOfComments!))
        
        
        if let urlString = post.postPreviewURL, let url = URL(string: urlString), URL.canOpenURL(string: urlString) {
            interactor.getImageAsync(from: url, success: { image in
                DispatchQueue.main.async {
                    cell.previewImage.image = image
                    cell.showPreview()
                }
            }) { errorMessage in
                
            }
        }else{
            cell.hidePreview()
        }
        
    }
    
    func endDisplayCell(cell: PostCell, post: PostModel) {
        if let urlString = post.postPreviewURL, let url = URL(string: urlString)  {
            interactor.cancelLoadImage(for: url)
        }
    }
    
    func setLastVisiblePosts(posts: [PostModel]){
        
        if(self.posts.count == 0){
            lastVisiblePosts = posts
        }else{
            findLastVisiblePosts()
        }
    }
    
    func findLastVisiblePosts(){
        
        var index = 0
        if lastVisiblePosts != nil, lastVisiblePosts!.count > 0 {
            for post in self.posts {
                if post.postID == lastVisiblePosts!.first?.postID {
                    index = self.posts.lastIndex(of: post)!
                    view.scrollTo(index: index)
                }
            }
        }
    }
    
    func canShowDetail(for post: PostModel) -> Bool {
        
        let canShow = (post.postPreviewURL != nil && post.postPreviewURL!.count > 0 && URL.canOpenURL(string: post.postPreviewURL!))
        return canShow
    }
    
    func didScrollToEnd() {
        interactor.loadNextPageData()
    }
    
    

}
