//
//  PostListInteractor.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

class PostListInteractor: PostListInteractorProtocol {
    
    weak var presenter: PostListPresenterProtocol!
    var apiManager: RedditAPI!
    var localDataManager: LocalDataManager!
    var imageWrapper: ImageWrapper!
    
    required init(presenter: PostListPresenterProtocol) {
        self.presenter = presenter
        apiManager = RedditAPI()
    }
    
    func loadData() {
        
        weak var weakSelf = self
        
        apiManager.getRedditTopPosts(success: { posts in
            DispatchQueue.main.async {
                weakSelf!.presenter.didLoadData(data: posts)
            }
        }) { errorMessage in
            DispatchQueue.main.async {
                weakSelf?.presenter.onError(message: errorMessage)
            }
        }
    }
    
    func loadNextPageData() {
        
        weak var weakSelf = self
        
        apiManager.getRedditTopPosts(after: apiManager.nextPage, success: { posts in
            DispatchQueue.main.async {
                weakSelf!.presenter.didLoadNextData(data: posts)
            }
        }) { errorMessage in
            DispatchQueue.main.async {
                weakSelf?.presenter.onError(message: errorMessage)
            }
        }
    }
    
    func getImageAsync(from url: URL,
                       success: @escaping downloadImageCompletionHandler,
                       failure: @escaping queryCompletionWithErrorMessage) {
        apiManager.downloadImage(from: url, success: success, failure: failure)
    }
    
    func cancelLoadImage(for url: URL){
        apiManager.cancelLoadImage(from: url)
    }
}
