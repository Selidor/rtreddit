//
//  PostListCoordinator.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

class PostListCoordinator: PostListRouterProtocol, PostWireframeProtocol {
    
    func configureModule(viewController: PostListViewController) {
        
        let presenter = PostListPresenter(view: viewController)
        let interactor = PostListInteractor(presenter: presenter)
        let localDataManager = LocalDataManager()
        let apiManager = RedditAPI()
        
        interactor.apiManager = apiManager
        interactor.localDataManager = localDataManager
        viewController.presenter = presenter;
        presenter.interactor = interactor;
        presenter.router = self;
    }
    
    func showDetailScene() {
        
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! DetailThumbnailViewController
        let sourceVC = segue.source as! PostListViewController
        destinationVC.selectedPost = sourceVC.selectedPost
    }
}
