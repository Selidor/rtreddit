//
//  DetailThumbnailViewController.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

class DetailThumbnailViewController: UIViewController, DetailThumbnailViewControllerProtocol {

    
    var presenter: DetailThumbnailPresenterProtocol!
    
    let coordinator: (DetailThumbnailWireframeProtocol & DetailThumbnailRouterProtocol) = DetailThumbnailCoordinator()
    
    var selectedPost: PostModel!
    
    @IBOutlet weak var sourceBarButton: UIBarButtonItem!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        
        coordinator.configureModule(viewController: self)
        activityIndicator.startAnimating()
        self.configurePost()
    }
    
    func configurePost() {
        if(selectedPost != nil){
            presenter.configureView(with: selectedPost)
            scrollView.contentSize = imageView.frame.size
            sourceBarButton.title = selectedPost.sourcePreviewURL
        }
    }
    
    func setImage(image: UIImage) {
        imageView.image = image
    }
    
    func showMessage(title: String!, description: String!) {
        
        let alertController = UIAlertController(title: title, message:
            description, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel))

        self.present(alertController, animated: true, completion: nil)
    }
    
    func startIndicator() {
        activityIndicator.startAnimating()
    }
    
    func stopIndicator() {
        activityIndicator.stopAnimating()
    }
    
    override func encodeRestorableState(with coder: NSCoder) {
        if let object = selectedPost {
            coder.encode(object, forKey: "selectedPost")
            coder.encode(scrollView.zoomScale, forKey: "zoomScale")
            coder.encode(NSCoder.string(for: scrollView.contentOffset) , forKey: "contentOffset")
            super.encodeRestorableState(with: coder)
        }
    }
    
    override func decodeRestorableState(with coder: NSCoder) {
        
        selectedPost = (coder.decodeObject(forKey: "selectedPost") as! PostModel)
        configurePost()
        scrollView.zoomScale = (coder.decodeObject(forKey: "zoomScale") as! CGFloat)
        scrollView.contentOffset = NSCoder.cgPoint(for: (coder.decodeObject(forKey: "contentOffset") as! String)) 
        
        super.decodeRestorableState(with: coder)
    }
    
    @IBAction func sourceAction(_ sender: Any) {
        presenter.openSourceURL()
    }
    @IBAction func saveAction(_ sender: Any) {
        presenter.saveActionDidTapped()
    }
}


extension DetailThumbnailViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}

extension DetailThumbnailViewController: UIViewControllerRestoration{
    static func viewController(withRestorationIdentifierPath identifierComponents: [String], coder: NSCoder) -> UIViewController? {
        let vc = DetailThumbnailViewController()
        return vc
    }
    
}
