//
//  DetailThumbnailPresenter.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

class DetailThumbnailPresenter: DetailThumbnailPresenterProtocol {
    
    weak var router: DetailThumbnailRouterProtocol!
    weak var view: DetailThumbnailViewControllerProtocol!
    var interactor: DetailThumbnailInteractorProtocol!
    var thumbnailImage: UIImage!
    
    required init(view: DetailThumbnailViewControllerProtocol) {
        self.view = view
    }
    
    func configureView(with post: PostModel!) {
        
        weak var weakSelf = self
        
        if let urlString = post.sourcePreviewURL, let url = URL(string: urlString){
            view.startIndicator()
            interactor.getImageAsync(from: url, success: { image in
                DispatchQueue.main.async {
                    weakSelf?.view.stopIndicator()
                    weakSelf?.thumbnailImage = image
                    weakSelf?.view.setImage(image: image)
                }
            }) { errorMessage in
                DispatchQueue.main.async {
                    weakSelf?.view.stopIndicator()
                    weakSelf?.view.showMessage(title: "Error", description: errorMessage)
                }
            }
        }else{
            view.showMessage(title: "Error", description: "Incorrect URL format")
        }
    }
    
    func openSourceURL(){
        
        let post = view.selectedPost
        if let urlString = post?.sourcePreviewURL, let url = URL(string: urlString){
            UIApplication.shared.open(url, options: [:]) { (Success) in
                
            }
        }else{
            view.showMessage(title: "Error", description: "Incorrect URL format")
        }
    }
    
    func saveActionDidTapped() {
        
        weak var weakSelf = self
        view.startIndicator()
        
        interactor.saveThumbnail(thumbnail: thumbnailImage) { (message, success) in
            DispatchQueue.main.async {
                weakSelf?.view.stopIndicator()
                let title = success ? "Success" : "Error"
                weakSelf?.view.showMessage(title: title, description: message)
            }
        }
    }
    
    
}
