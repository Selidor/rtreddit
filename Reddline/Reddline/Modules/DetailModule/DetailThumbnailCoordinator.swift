//
//  DetailThumbnailCoordinator.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

class DetailThumbnailCoordinator: DetailThumbnailRouterProtocol, DetailThumbnailWireframeProtocol {
    
    private weak var viewController: DetailThumbnailViewController!
    
    func dismiss() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    func configureModule(viewController: DetailThumbnailViewController) {
        
        let presenter = DetailThumbnailPresenter(view: viewController)
        let interactor = DetailThumbnailInteractor(presenter: presenter)
        
        viewController.presenter = presenter;
        presenter.interactor = interactor;
        presenter.router = self;
        self.viewController = viewController
    }
    
}
