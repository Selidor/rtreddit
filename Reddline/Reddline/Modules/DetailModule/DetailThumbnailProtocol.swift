//
//  DetailThumbnailProtocol.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.

import UIKit

typealias completionWithMessage = (_ message:String, _ success: Bool) -> Void

protocol DetailThumbnailViewControllerProtocol: class {
    
    var selectedPost: PostModel! { set get }
    
    func setImage(image: UIImage)
    
    func showMessage(title: String!, description: String!)
    
    func startIndicator()
    
    func stopIndicator()
}

protocol DetailThumbnailPresenterProtocol: class {
    
    var thumbnailImage: UIImage! { get }
    
    func configureView(with post: PostModel!)
    
    func openSourceURL()
    
    func saveActionDidTapped()

}

protocol DetailThumbnailInteractorProtocol: class {
    
    func getImageAsync(from url: URL, success: @escaping downloadImageCompletionHandler, failure: @escaping queryCompletionWithErrorMessage)
    
    func saveThumbnail(thumbnail: UIImage!, competion: @escaping completionWithMessage)
    
}

protocol DetailThumbnailRouterProtocol: class {
    
    func dismiss()
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
}

protocol DetailThumbnailWireframeProtocol: class {

    func configureModule(viewController: DetailThumbnailViewController)
}
