//
//  DetailThumbnailInteractor.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit
import Photos

class DetailThumbnailInteractor: DetailThumbnailInteractorProtocol {
    
    var apiManager: RedditAPI! = RedditAPI()
    weak var presenter: DetailThumbnailPresenterProtocol!
    
    required init(presenter: DetailThumbnailPresenterProtocol) {
        self.presenter = presenter
        apiManager = RedditAPI()
    }
    
    func saveThumbnail(thumbnail: UIImage!, competion: @escaping completionWithMessage) {
        
        weak var weakSelf = self
        
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.notDetermined {
            
            PHPhotoLibrary.requestAuthorization { (status) in
                if status == PHAuthorizationStatus.authorized{
                    weakSelf!.saveThumbnail(thumbnail: thumbnail, competion: competion)
                }else{
                    competion("Photos authorization failed", false)
                }
            }
            return
        }
        PHPhotoLibrary.shared().performChanges({
            PHAssetCreationRequest.creationRequestForAsset(from: thumbnail)
        }) { (success, error) in
            if (success) {
                competion("Photo have been saved", true)
            }
            else {
                competion(error?.localizedDescription ?? "Photo not saved for unlnown reason", false)
            }
        }
    }
    
    func getImageAsync(from url: URL, success: @escaping downloadImageCompletionHandler, failure: @escaping queryCompletionWithErrorMessage) {
        apiManager.downloadImage(from: url, success: success, failure: failure)
    }

}
