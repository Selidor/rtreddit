//
//  PostModel.swift
//  Reddline
//
//  Created by Artem Shvets on 20.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit
import Foundation

enum CodingKeys : String, CodingKey {
    
    case created
    case numOfComments
    case postID
    case postPreviewURL
    case sourcePreviewURL
    case author
    case title
}

class PostModel: NSObject, NSCoding{
    
    var created: Date! = nil
    var numOfComments: Int! = 0
    var postID: String? = nil
    var postPreviewURL: String? = nil
    var sourcePreviewURL: String? = nil
    var author: String? = nil
    var title: String? = nil
    
    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: CodingKeys.title.rawValue)
        coder.encode(created, forKey: CodingKeys.created.rawValue)
        coder.encode(numOfComments, forKey: CodingKeys.numOfComments.rawValue)
        coder.encode(postID, forKey: CodingKeys.postID.rawValue)
        coder.encode(postPreviewURL, forKey: CodingKeys.postPreviewURL.rawValue)
        coder.encode(sourcePreviewURL, forKey: CodingKeys.sourcePreviewURL.rawValue)
        coder.encode(author, forKey: CodingKeys.author.rawValue)
    }
    
    convenience required  init?(coder: NSCoder) {
        
        let title = coder.decodeObject(forKey: CodingKeys.title.rawValue) as? String
        let created = coder.decodeObject(forKey: CodingKeys.created.rawValue) as! Date
        let numOfComments = coder.decodeObject(forKey: CodingKeys.numOfComments.rawValue) as! Int
        let postID = coder.decodeObject(forKey: CodingKeys.postID.rawValue) as? String
        let postPreviewURL = coder.decodeObject(forKey: CodingKeys.postPreviewURL.rawValue) as? String
        let sourcePreviewURL = coder.decodeObject(forKey: CodingKeys.sourcePreviewURL.rawValue) as? String
        let author = coder.decodeObject(forKey: CodingKeys.author.rawValue) as? String
        
        self.init(title: title,
                  created: created,
                  numOfComments: numOfComments,
                  postID: postID,
                  postPreviewURL: postPreviewURL,
                  sourcePreviewURL: sourcePreviewURL,
                  author: author)
        
    }
    
    init(title: String?,
         created: Date!,
         numOfComments: Int!,
         postID: String?,
         postPreviewURL: String?,
         sourcePreviewURL: String?,
         author: String?){
        
        super.init()
        self.title = title
        self.created = created
        self.numOfComments = numOfComments
        self.postID = postID
        self.sourcePreviewURL = sourcePreviewURL
        self.postPreviewURL = postPreviewURL
        self.author = author
    }
}
