//
//  PostModelMapper.swift
//  Reddline
//
//  Created by Artem Shvets on 20.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

class PostModelMapper: NSObject {
    
    enum SerializationError: Error {
        case missing(String)
        case invalid(String, Any)
    }

    func map(redditPost: Children) -> PostModel? {
        
        let sourcePreviewURL = redditPost.data.preview?.images.first?.resolutions.last?.url?.replacingOccurrences(of: "&amp;", with: "&")
        
        let createdDate = Date.init(timeIntervalSince1970: redditPost.data.created)
        let postModel = PostModel(title: redditPost.data.title,
                                  created: createdDate,
                                  numOfComments: redditPost.data.numOfComments,
                                  postID: redditPost.data.id,
                                  postPreviewURL: redditPost.data.thumbnail,
                                  sourcePreviewURL: sourcePreviewURL,
                                  author: redditPost.data.author)
        
        return postModel
    }
    
    func map(posts: RedditData) -> [PostModel] {
        
        var postsModels = Array<PostModel>()
        let postList = posts.data.children;
        
        for (_, element) in postList.enumerated() {
            let model = map(redditPost: element)
            if model != nil {
                postsModels.append(model!)
            }
        }
        
        return postsModels
    }
    
    func convert(fromManagedObject managedObject: Post) -> PostModel {
        
        let date = Date.init(timeIntervalSince1970: managedObject.created)
        
        let postModel = PostModel(title: managedObject.title,
                                  created: date,
                                  numOfComments: Int(managedObject.num_comments),
                                  postID: managedObject.post_id,
                                  postPreviewURL: managedObject.post_thumbnail_url,
                                  sourcePreviewURL: managedObject.post_preview_url,
                                  author: managedObject.author)
        
        return postModel
    }
    
    func convert(fromManagedObjects managedObjects: [Post]) -> [PostModel] {
        
        var postsModels = Array<PostModel>()
        
        for Post in managedObjects {
            postsModels.append(convert(fromManagedObject: Post))
        }
        
        return postsModels
    }
}
