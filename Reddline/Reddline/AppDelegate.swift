//
//  AppDelegate.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit
import CoreData


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var coreDataStore: CoreDataStore?
    var window: UIWindow?

    class func sharedInstance() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate;
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupAppearence()
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        coreDataStore?.saveContext()
    }
    
    func application(_ application: UIApplication, viewControllerWithRestorationIdentifierPath identifierComponents: [String], coder: NSCoder) -> UIViewController? {
        
        return nil
    }
    
    func application(_ application: UIApplication, shouldSaveApplicationState coder: NSCoder) -> Bool {
      return true
    }
    
    func application(_ application: UIApplication, shouldRestoreApplicationState coder: NSCoder) -> Bool {
      return true
    }
    
    func setupAppearence(){
        UINavigationBar.appearance().barTintColor = .redditColor()
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
    }

}

