//
//  RedditAPI.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit
import Foundation
import AuthenticationServices



class RedditAPI {
    
    private let networkManager = NetworkManager()
    private let postModelMapper = PostModelMapper()
    
    private let authorizationURL = "https://ssl.reddit.com/api/v1/authorize"
    private let api = "https://www.reddit.com"
    private let topJSONEndpoint = "/top.json"
    private let nextPageQuery = "after"
    
    private let clientId = "Fa9UKQ8La1Kauw"
    private let responseType = "code"
    private let state = "TEST"
    private let redirectUri = "reddline://oauth-callback"
    private let duration = "permanent"
    private let scope = "read"
    
    private let clientIdKey = "client_id"
    private let responseTypeKey = "response_type"
    private let stateKey = "state"
    private let redirectUriKey = "redirect_uri"
    private let durationKey = "duration"
    private let scopeKey = "scope"
    
    private var authSession: ASWebAuthenticationSession! = nil
    
    var nextPage: String?
    
    typealias JSONStructure = Array<Dictionary<String, Any>>
    typealias queryCompletionHandler = (Array<PostModel>) -> Void
    typealias downloadImageCompletionHandler = (UIImage) -> Void
    typealias queryCompletionWithErrorMessage = (String) -> Void
    
    func getRedditTopPosts(after: String?,success: @escaping queryCompletionHandler,
                           failure: @escaping queryCompletionWithErrorMessage){
        weak var weakSelf = self
        
        if let redditUrlComponents = NSURLComponents(string: api + topJSONEndpoint){
            
            if (after != nil) {
                let querryItems = [NSURLQueryItem (name: nextPageQuery, value: after)]
                redditUrlComponents.queryItems = querryItems as [URLQueryItem]
            }
            
            networkManager.getMethod(url: redditUrlComponents.url!, success: { data in
                let decoder = JSONDecoder()
                do{
                    let res = try decoder.decode(RedditData.self, from: data)
                    weakSelf?.nextPage = res.data.after
                    let postModels = weakSelf?.postModelMapper.map(posts: res)
                    success(postModels ?? [])
                }catch{
                    failure("JSON Serialization Error")
                }
                
            }) { error in
                failure(error?.localizedDescription ?? "")
            }
        }
    }
    
    func cancelLoadImage(from url:URL){
        networkManager.cancelTask(at: url)
    }
    
    func getRedditTopPosts(success: @escaping queryCompletionHandler,
                           failure:@escaping queryCompletionWithErrorMessage) {
        nextPage = ""
        getRedditTopPosts(after: nil, success: success, failure: failure)
    }
    
    func downloadImage(from url: URL,
                       success: @escaping downloadImageCompletionHandler,
                       failure: @escaping queryCompletionWithErrorMessage) {
        
        networkManager.getMethod(url: url, success: { data in
            
            let image = UIImage(data: data);
            if (image != nil) {
                success(image!)
            }else{
                failure("")
            }
        }) { error in
            failure(error?.localizedDescription ?? "")
        }
    }
    
}
