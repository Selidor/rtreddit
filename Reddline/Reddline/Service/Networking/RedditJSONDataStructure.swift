//
//  RedditJSONDataStructure.swift
//  Reddline
//
//  Created by Artem Shvets on 25.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

struct RedditData : Codable {

    let kind: String
    let data: PrimaryData
}

struct PrimaryData : Codable{
    
    let dist: Int
    let children: [Children]
    let after: String?
    let before: String?
}

struct Children : Codable {
    
    let kind: String
    let data: SecondaryData
}

struct SecondaryData : Codable{
    enum CodingKeys : String, CodingKey {
        
        case numOfComments = "num_comments"
        case title
        case author
        case id
        case created
        case thumbnail
        case preview
    }
    
    let title: String
    let author: String
    let id: String
    
    let numOfComments: Int
    let created: Double
    
    let thumbnail: String?
    let preview: Preview?

    init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
        numOfComments = try container.decode(Int.self, forKey: .numOfComments)
        title = try container.decode(String.self, forKey: .title)
        author = try container.decode(String.self, forKey: .author)
        id = try container.decode(String.self, forKey: .id)
        created = try container.decode(Double.self, forKey: .created)
        preview = try? container.decode(Preview.self, forKey: .preview)
        thumbnail = try? container.decode(String.self, forKey: .thumbnail)
    }
    
}

struct Preview : Codable {
   
    let images: [Image]
    let enabled: Bool
}

struct Image: Codable {
       let resolutions: [Source]
}

struct Source : Codable {
    let url: String?
    let width: Int?
    let height: Int?
}
