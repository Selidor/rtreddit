//
//  NetworkManager.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import Foundation

typealias querySuccessHandler = (Data) -> Void
typealias queryFailureHandler = (Error?) -> Void

struct TaskHandler {
    let successHandler: querySuccessHandler
    let failureHandler: queryFailureHandler
}

class NetworkManager {
    
    private let urlSession = URLSession(configuration: .default)
    var taskHandlersURLs = [URL: [TaskHandler]]()
    var taskURLs = [URL: URLSessionDataTask]()
    let lock = NSLock()
    
    func getMethod(url: URL, success: @escaping querySuccessHandler, failure: @escaping queryFailureHandler) {
        
        let taskHandler = TaskHandler(successHandler: success, failureHandler: failure)
        if taskHandlersURLs.keys.contains(url) {
            taskHandlersURLs[url]?.append(taskHandler)
        }else{
            taskHandlersURLs[url] = [taskHandler]
            
            let task = urlSession.dataTask(with: url) { data, response, error in
                
                guard let taskHandlers = self.taskHandlersURLs[url] else { return }
                
                for handler in taskHandlers {
                    
                    if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                        handler.successHandler(data)
                    }else{
                        handler.failureHandler(error)
                    }
                }
                
                self.lock.lock()
                
                self.taskHandlersURLs.removeValue(forKey: url);
                self.taskURLs.removeValue(forKey: url)
                
                self.lock.unlock()
                
            }
            
            taskURLs[url] = task
            task.resume()
        }
    }
    
    func cancelTask(at url: URL){
        
        let task = taskURLs[url]
        if(task?.state == URLSessionTask.State.running){
            task?.cancel()
        }
        taskURLs.removeValue(forKey: url)
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}
