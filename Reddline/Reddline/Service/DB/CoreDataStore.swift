//
//  CoreDataStore.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit
import CoreData

class CoreDataStore: NSObject {
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "ReddlineDB")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    var persistentStoreCoordinator: NSPersistentStoreCoordinator? {
        return persistentContainer.persistentStoreCoordinator
    }
    
    var managedObjectModel: NSManagedObjectModel? {
        return persistentContainer.managedObjectModel
    }
    
    var managedObjectContext: NSManagedObjectContext? {
        return persistentContainer.viewContext
    }
}
