//
//  App+Color.swift
//  Reddline
//
//  Created by Artem Shvets on 04.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

extension UIColor {
    class func redditColor() -> UIColor {
        return UIColor.init(red: 255.0/255.0, green: 69.0/255.0, blue: 1/255.0, alpha: 1.0)
    }
}
