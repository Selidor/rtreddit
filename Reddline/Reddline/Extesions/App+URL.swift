//
//  App+URL.swift
//  Reddline
//
//  Created by Artem Shvets on 25.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit

extension URL {
    static func canOpenURL(string: String?) -> Bool {
        guard string != nil else {return false}
        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with: string)
    }
}
